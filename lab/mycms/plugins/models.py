from django.db import models
from cms.models import CMSPlugin


class LikePlugin(CMSPlugin):
    href = models.CharField(max_length=200)
    share = models.BooleanField()
    show_faces = models.BooleanField()
    LAYOUT = (
        ('standard', 'Standard'),
        ('button_count', 'Button Count'),
        ('button', 'Button'),
        ('box_count', 'Box Count'),
    )
    layout = models.CharField(max_length=20, choices=LAYOUT)
    ACTION = (
        ('like', 'Like'),
        ('recommended', 'Recommended'),
    )
    action = models.CharField(max_length=20, choices=ACTION)