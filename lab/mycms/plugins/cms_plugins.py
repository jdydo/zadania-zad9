from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from models import *
from django.utils.translation import ugettext as _


class CMSLikePlugin(CMSPluginBase):
    model = LikePlugin
    name = _("Like Plugin")
    render_template = "plugins/plugin_like.html"

    def render(self, context, instance, placeholder):
        context.update({'instance': instance})
        return context

plugin_pool.register_plugin(CMSLikePlugin)


