# -*- coding=utf-8 -*-

from django.test.client import Client
from django.test.utils import setup_test_environment
from datetime import timedelta
from cms.api import create_page
from django.test.utils import override_settings
from cms.test_utils.testcases import CMSTestCase
from menu import *


setup_test_environment()
client = Client()


@override_settings(ROOT_URLCONF='cms_project.test_urls')
class MicroblogTest(CMSTestCase):
    def save(self):
        self.tag1.save()
        self.tag2.save()
        self.post1.save()
        self.post2.save()

    def setUp(self):
        user0 = User.objects.create_user(
            username='Garrus', password='Garrus', email='admind@citadel.com', first_name='Admin', last_name='Admin'
        )
        user0.save()
        user0.is_staff = True
        user0.is_superuser = True
        user0.save()

        self.user1 = User.objects.create_user(
            username='Shepard', password='Shepard', email='shepard@citadel.com', first_name='Commander',
            last_name='Shepard'
        )
        self.user2 = User.objects.create_user(
            username='Miranda', password='Miranda', email='miranda@citadel.com', first_name='Miranda',
            last_name='Lawson'
        )

        self.tag1 = Tags(name='Citadel')
        self.tag2 = Tags(name='Star Fleet')

        self.post1 = Posts(
            author=self.user1, title='Some interesting Title1', text='Some interesting Text1',
            date=timezone.now()
        )
        self.post2 = Posts(
            author=self.user2, title='Some interesting Title2', text='Some interesting Text2',
            date=timezone.now()
        )

    def add_many_to_many(self):
        self.post1.tags.add(self.tag1)
        self.post2.tags.add(self.tag2)

        ##############
        ###  Baza  ###
        ##############

    def test_empty_post_base(self):
        assert Posts.objects.count() == 0

    def test_empty_tag_base(self):
        assert Tags.objects.count() == 0

    def test_filled_user_base(self):
        self.user1.save()
        assert User.objects.count() == 3

    def test_filled_post_base(self):
        self.save()
        assert Posts.objects.count() == 2

    def test_filled_tags_base(self):
        self.save()
        assert Tags.objects.count() == 2

    def test_model(self):
        self.save()
        post = Posts.objects.filter(id=1)[0]
        assert 'Shepard' == post.author.username
        assert 'Some interesting Title1' == post.title
        assert 'Some interesting Text1' == post.text

        ########################################
        ###  Wyswietlanie wszystkich wpisow  ###
        ########################################

    def test_get_empty_blog(self):
        response = client.get('/blog/')
        assert response.status_code == 200
        assert len(response.context['posts']) == 0

    def test_get_filled_blog(self):
        self.save()
        response = client.get('/blog/')
        assert response.status_code == 200
        assert len(response.context['posts']) == 2

    def test_tags_display(self):
        self.save()
        self.add_many_to_many()
        response = client.get('/blog/')
        assert 'Citadel' in response.content
        assert 'Star Fleet' in response.content

        ################
        ###  Filtry  ###
        ################

    def test_get_months_list(self):
        self.save()
        response = client.get('/blog/filter/month/')
        assert 'Some interesting Title1' in response.content
        assert 'Some interesting Title2' in response.content
        assert '/blog/filter/post/1' in response.content
        assert '/blog/filter/post/2' in response.content

    def test_get_months_post(self):
        self.save()
        response = client.get('/blog/filter/month/%s/' % timezone.now().month)
        assert response.status_code == 200
        assert 'Some interesting Title1' in response.content
        assert 'Some interesting Title2' in response.content
        assert 'Some interesting Text1' in response.content
        assert 'Some interesting Text2' in response.content
        assert '/blog/filter/post/1' in response.content
        assert '/blog/filter/post/2' in response.content

    def test_get_all_post(self):
        self.save()
        response = client.get('/blog/filter/all/')
        assert response.status_code == 200
        assert 'Some interesting Title1' in response.content
        assert 'Some interesting Title2' in response.content
        assert 'Some interesting Text1' in response.content
        assert 'Some interesting Text2' in response.content
        assert '/blog/filter/post/1' in response.content
        assert '/blog/filter/post/2' in response.content

    def test_get_user_list(self):
        self.save()
        response = client.get('/blog/filter/user/')
        assert response.status_code == 200
        assert 'Miranda' in response.content
        assert 'Shepard' in response.content

    def test_get_user_posts(self):
        self.save()
        response = client.get('/blog/filter/user/2/')
        assert response.status_code == 200
        assert 'Shepard' in response.content
        assert 'Some interesting Title1' in response.content
        assert 'Some interesting Text1' in response.content
        assert 'Miranda Lawson' not in response.content
        assert 'Some interesting Title2' not in response.content
        assert 'Some interesting Text2' not in response.content

    def test_get_tags_list(self):
        self.save()
        self.add_many_to_many()
        response = client.get('/blog/filter/tag/')
        assert response.status_code == 200
        assert 'Citadel' in response.content
        assert 'Star Fleet' in response.content

    def test_get_tags_posts(self):
        self.save()
        self.add_many_to_many()
        response = client.get('/blog/filter/tag/1/')
        assert response.status_code == 200
        assert 'Shepard' in response.content
        assert 'Some interesting Title1' in response.content
        assert 'Some interesting Text1' in response.content
        assert 'Miranda Lawson' not in response.content
        assert 'Some interesting Title2' not in response.content
        assert 'Some interesting Text2' not in response.content

    def test_empty_user_context(self):
        assert len(client.get('/blog/filter/user/7/').context['posts']) == 0

    def test_empty_tag_context(self):
        assert len(client.get('/blog/filter/tag/7/').context['posts']) == 0

    def test_empty_date_context(self):
        assert len(client.get('/blog/filter/tag/13/').context['posts']) == 0

        #############
        ##  Menu  ###
        #############

    def test_blog_menu(self):
        nodes = BlogMenu().get_nodes(None)
        assert len(nodes) == 2
        assert nodes[0].title == 'Pokaz wpisy'
        assert nodes[0].url == '/blog/'
        assert nodes[1].title == 'Dodaj'
        assert nodes[1].url == '/blog/add/'

    def test_filtr_menu(self):
        nodes = FiltrMenu().get_nodes(None)
        assert len(nodes) == 4
        assert nodes[0].title == 'Wszystkie posty'
        assert nodes[0].url == '/blog/filter/all/'
        assert nodes[1].title == 'Po uzytkownikach'
        assert nodes[1].url == '/blog/filter/user/'
        assert nodes[2].title == 'Po miesiacu'
        assert nodes[2].url == '/blog/filter/month/'
        assert nodes[3].title == 'Po tagu'
        assert nodes[3].url == '/blog/filter/tag/'

    def test_tag_menu(self):
        self.save()
        self.add_many_to_many()
        nodes = TagMenu().get_nodes(None)
        assert len(nodes) == 6
        assert nodes[0].title == 'Citadel'
        assert nodes[0].url == '/blog/filter/tag/1/'
        assert nodes[2].title == 'Some interesting Title1'
        assert '/filter/post/1/' in nodes[2].url
        assert nodes[2].parent_id == 1
        assert nodes[3].title == 'Star Fleet'
        assert nodes[3].url == '/blog/filter/tag/2/'
        assert nodes[5].title == 'Some interesting Title2'
        assert '/filter/post/2/' in nodes[5].url
        assert nodes[5].parent_id == 2

    def test_post_menu(self):
        self.save()
        nodes = PostMenu().get_nodes(None)
        assert len(nodes) == 2
        assert nodes[0].title == 'Some interesting Title2'
        assert nodes[1].title == 'Some interesting Title1'
        assert '/filter/post/2' in nodes[0].url
        assert '/filter/post/1/' in nodes[1].url


    def test_user_menu(self):
        self.save()
        nodes = UserMenu().get_nodes(None)
        assert len(nodes) == 8
        assert nodes[0].title == 'Garrus'
        assert nodes[2].title == 'Miranda'
        assert nodes[5].title == 'Shepard'
        assert nodes[0].url == '/blog/filter/user/1/'
        assert nodes[2].url == '/blog/filter/user/3/'
        assert nodes[5].url == '/blog/filter/user/2/'
        assert nodes[7].title == 'Some interesting Title1'
        assert nodes[4].title == 'Some interesting Title2'
        assert '/filter/post/2/' in nodes[4].url
        assert '/filter/post/1/' in nodes[7].url
        assert nodes[4].parent_id == 3
        assert nodes[7].parent_id == 2

    def test_month_menu(self):
        self.save()
        nodes = MonthMenu().get_nodes(None)
        assert len(nodes) == 26
        months = ['Styczen', 'Luty', 'Marzec', 'Kwiecien', 'Maj', 'Czerwiec', 'Lipiec', 'Sierpien', 'Wrzesien', 'Pazdziernik', 'Listopad', 'Grudzien']
        months_url = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12']
        for i in range(12):
            assert nodes[i].title == months[i]
            assert nodes[i].url == '/blog/filter/month/%s/' % months_url[i]
        assert nodes[24].title == 'Some interesting Title2'
        assert nodes[25].title == 'Some interesting Title1'
        assert '/filter/post/2/' in nodes[24].url
        assert '/filter/post/1/' in nodes[25].url
        assert nodes[24].parent_id == timezone.now().month
        assert nodes[25].parent_id == timezone.now().month

        ###################
        ###  Logowanie  ###
        ###################

    def test_get_login(self):
        response = client.get('/blog/login/')
        assert response.status_code == 200
        assert 'name="username"' in response.content
        assert 'name="password"' in response.content

    def test_post_login_with_valid_data(self):
        self.save()
        response = client.post('/blog/login/', {'username': 'Shepard', 'password': 'Shepard'}, follow=True)
        assert response.status_code == 200
        assert 'Zalogowano!' in response.content

    def test_post_login_with_invalid_data(self):
        self.save()
        response = client.post('/blog/login/', {'username': 'bad', 'password': 'bad'}, follow=True)
        assert response.status_code == 200
        assert 'Zle haslo lub login!' in response.content
        assert 'name="username"' in response.content
        assert 'name="password"' in response.content

        ##########################
        ###  Dodawanie wpisow  ###
        ##########################

    def test_get_add_without_login(self):
        response = client.get('/blog/add/', follow=True)
        assert response.status_code == 200
        assert 'name="username"' in response.content
        assert 'name="password"' in response.content

    def test_get_add_with_login(self):
        self.save()
        client.post('/blog/login/', {'username': 'Shepard', 'password': 'Shepard'})
        response = client.get('/blog/add/')
        assert response.status_code == 200
        assert 'name="title"' in response.content
        assert 'name="text"' in response.content
        assert 'name="author" type="hidden"' in response.content

    def test_post_add_without_login(self):
        response = client.post('/blog/add/', {'title': 'Some Title', 'text': 'Some Text'}, follow=True)
        assert response.status_code == 200
        assert 'name="username"' in response.content
        assert 'name="password"' in response.content

    def test_post_add_with_login_and_valid_data(self):
        create_page(
            title='Blog',
            language='pl',
            slug='/blog/',
            template='fullwidth.html',
            publication_date=timezone.now(),
            in_navigation=True,
            published=True
        )
        self.save()
        client.login(username='Shepard', password='Shepard')
        response = client.post('/blog/add/', {'title': 'Some interesting Title3', 'text': 'Some interesting Text3'})
        assert response.status_code == 200
        assert 'Wpis dodany!' in response.content
        response = client.get('/blog/')
        assert len(response.context['posts']) == 3
        assert 'Some interesting Text3' in response.content
        assert 'Some interesting Title3' in response.content

    def test_post_add_with_login_and_empty_data(self):
        self.save()
        client.post('/blog/login/', {'username': 'Shepard', 'password': 'Shepard'})
        response = client.post('/blog/add/', {'title': '', 'text': ''}, follow=True)
        assert response.status_code == 200
        assert 'To pole jest wymagane.' in response.content
        assert 'name="title"' in response.content
        assert 'name="text"' in response.content

    def test_post_add_with_login_and_invalid_data(self):
        self.save()
        client.post('/blog/login/', {'username': 'Shepard', 'password': 'Shepard'})
        response = client.post('/blog/add/', {'title': 'aa', 'text': 'bb'}, follow=True)
        assert response.status_code == 200
        assert 'Tytul powinien miec od 5 do 50 znakow.' in response.content
        assert 'Wpis powinien miec od 10 do 1000 znakow.' in response.content
        assert 'name="title"' in response.content
        assert 'name="text"' in response.content

        #####################
        ###  Wylogowanie  ###
        #####################

    def test_get_logout(self):
        self.save()
        client.post('/blog/login/', {'username': 'Shepard', 'password': 'Shepard'})
        response = client.get('/blog/logout/', follow=True)
        assert response.status_code == 200
        assert 'name="username"' in response.content
        assert 'name="password"' in response.content

        ###########################
        ###  Edytowanie wpisow  ###
        ###########################

    def test_get_edit_without_login(self):
        self.post1.date = self.post1.date - timedelta(minutes=10)
        self.save()
        response = client.get('/blog/edit/1/', follow=True)
        assert 'name="username"' in response.content
        assert 'name="password"' in response.content

    def test_get_edit_with_login_within_time(self):
        self.save()
        client.login(username='Miranda', password='Miranda')
        response = client.get('/blog/edit/2/')
        assert response.status_code == 200
        assert 'name="title"' in response.content
        assert 'name="text"' in response.content
        assert 'name="date" type="hidden"' in response.content
        assert 'Some interesting Text2' in response.content
        assert 'Some interesting Title2' in response.content

    def test_get_edit_with_login_within_time_other_post(self):
        self.save()
        client.login(username='Miranda', password='Miranda')
        response = client.get('/blog/edit/1/', follow=True)
        assert response.status_code == 200
        assert 'Brak dostepu!' in response.content

    def test_get_edit_with_login_without_time(self):
        self.post2.date = self.post2.date - timedelta(minutes=10)
        self.save()
        client.login(username='Miranda', password='Miranda')
        response = client.get('/blog/edit/2/', follow=True)
        assert response.status_code == 200
        assert 'Brak dostepu!' in response.content

    def test_post_edit_without_login(self):
        self.save()
        response = client.post('/blog/edit/2/', {'title': 'Some interesting Title3', 'text': 'Some interesting Text3'}, follow=True)
        assert response.status_code == 200
        assert 'name="username"' in response.content
        assert 'name="password"' in response.content