# -*- coding: utf-8 -*-

from django.utils.translation import ugettext_lazy as _
from cms.menu_bases import CMSAttachMenu
from menus.base import NavigationNode
from menus.menu_pool import menu_pool
from models import Posts, Tags
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.utils import timezone


class BlogMenu(CMSAttachMenu):
    name = _("Blog Menu")
    cache = False

    def get_nodes(self, request):
        nodes = list()
        nodes.append(NavigationNode(_('Pokaz wpisy'), "/blog/", 'main'))
        nodes.append(NavigationNode(_('Dodaj'), "/blog/add/", 'add', attr={'visible_for_anonymous': False}))
        return nodes


class FiltrMenu(CMSAttachMenu):
    name = _("Filtr Menu")
    cache = False

    def get_nodes(self, request):
        nodes = list()
        nodes.append(NavigationNode(_('Wszystkie posty'), "/blog/filter/all/", 'all'))
        nodes.append(NavigationNode(_('Po uzytkownikach'), "/blog/filter/user/", 'user'))
        nodes.append(NavigationNode(_('Po miesiacu'), "/blog/filter/month/", 'month'))
        nodes.append(NavigationNode(_('Po tagu'), "/blog/filter/tag/", 'tag'))
        return nodes


class UserMenu(CMSAttachMenu):
    name = _("Users Menu")
    cache = False

    def get_nodes(self, request):
        nodes = list()
        for user in User.objects.all().order_by("username"):
            nodes.append(NavigationNode(user.username, "/blog/filter/user/%s/" % user.pk, user.pk))

            nodes.append(NavigationNode(_('--- Pokaz wszystkie wpisy ---'), "/blog/filter/user/%s/" % str(user.pk),
                                        'user%s' % str(user.pk), user.pk))

            for post in Posts.objects.all().filter(author=user):
                nodes.append(NavigationNode(title=post.title, url=reverse('show_post', args=(post.pk,)),
                                            id='post' + str(post.pk), parent_id=user.pk))
        return nodes


class TagMenu(CMSAttachMenu):
    name = _("Tag Menu")
    cache = False

    def get_nodes(self, request):
        nodes = list()
        for tag in Tags.objects.all().order_by("name"):
            nodes.append(NavigationNode(tag.name, "/blog/filter/tag/%s/" % tag.pk, tag.pk))

            nodes.append(NavigationNode(_('--- Pokaz wszystkie wpisy ---'), "/blog/filter/tag/%s/" % str(tag.pk),
                                        'user%s' % str(tag.pk), tag.pk))

            for post in Posts.objects.all().filter(tags__id=tag.id).order_by('-id'):
                nodes.append(NavigationNode(title=post.title, url=reverse('show_post', args=(post.pk,)),
                                            id='post' + str(post.pk), parent_id=tag.id))
        return nodes


class MonthMenu(CMSAttachMenu):
    name = _("Month Menu")
    cache = False

    def get_nodes(self, request):
        nodes = list()
        nodes.append(NavigationNode(_('Styczen'), "/blog/filter/month/1/", 1))
        nodes.append(NavigationNode(_('Luty'), "/blog/filter/month/2/", 2))
        nodes.append(NavigationNode(_('Marzec'), "/blog/filter/month/3/", 3))
        nodes.append(NavigationNode(_('Kwiecien'), "/blog/filter/month/4/", 4))
        nodes.append(NavigationNode(_('Maj'), "/blog/filter/month/5/", 5))
        nodes.append(NavigationNode(_('Czerwiec'), "/blog/filter/month/6/", 6))
        nodes.append(NavigationNode(_('Lipiec'), "/blog/filter/month/7/", 7))
        nodes.append(NavigationNode(_('Sierpien'), "/blog/filter/month/8/", 8))
        nodes.append(NavigationNode(_('Wrzesien'), "/blog/filter/month/9/", 9))
        nodes.append(NavigationNode(_('Pazdziernik'), "/blog/filter/month/10/", 10))
        nodes.append(NavigationNode(_('Listopad'), "/blog/filter/month/11/", 11))
        nodes.append(NavigationNode(_('Grudzien'), "/blog/filter/month/12/", 12))

        for i in range(12):
            nodes.append(NavigationNode(_('--- Pokaz wszystkie wpisy ---'), "/blog/filter/month/%s/" % str(i + 1),
                                        'month%s' % str(i + 1), i + 1))

        for post in Posts.objects.all().filter(date__year=timezone.now().year).order_by("-pk"):
            nodes.append(NavigationNode(post.title, reverse('show_post', args=(post.pk,)), 'post' + str(post.pk),
                                        post.date.month))
        return nodes


class PostMenu(CMSAttachMenu):
    name = _("Post Menu")
    cache = False

    def get_nodes(self, request):
        nodes = list()
        for post in Posts.objects.all().order_by("-pk"):
            nodes.append(NavigationNode(post.title, reverse('show_post', args=(post.pk,)), post.pk))
        return nodes


menu_pool.register_menu(TagMenu)
menu_pool.register_menu(FiltrMenu)
menu_pool.register_menu(PostMenu)
menu_pool.register_menu(MonthMenu)
menu_pool.register_menu(UserMenu)
menu_pool.register_menu(BlogMenu)
