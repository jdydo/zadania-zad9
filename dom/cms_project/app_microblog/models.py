from django.contrib.auth.models import User
from django.db import models
from cms.models import CMSPlugin


class Tags(models.Model):
    name = models.CharField(max_length=20, verbose_name='Nazwa')

    def __unicode__(self):
        return self.name


class Posts(models.Model):
    author = models.ForeignKey(User, blank=True, null=True, related_name='Author', verbose_name='Autor')
    title = models.CharField(max_length=200, verbose_name='Tytul')
    text = models.TextField(verbose_name='Wpis')
    date = models.DateTimeField(blank=True, null=True, verbose_name='Data')
    tags = models.ManyToManyField(Tags, blank=True, null=True, verbose_name='Tagi')
    last_edited_date = models.DateTimeField(blank=True, null=True, verbose_name='Ostatnio zaktualizowano')
    last_edited_by = models.ForeignKey(User, blank=True, null=True,  related_name='Last edited by', verbose_name='Ostatnio zaktualizowal')


class PostsPlugin(CMSPlugin):
    posts = Posts()

    def refresh(self):
        self.posts = Posts.objects.all().order_by('-id').first()


class TagsPlugin(CMSPlugin):
    tags = Tags.objects.all().order_by('name')

    def refresh(self):
        self.tags = Tags.objects.all().order_by('name')


class UserPlugin(CMSPlugin):
    users = User.objects.all().order_by('username')

    def refresh(self):
        self.users = User.objects.all().order_by('username')