from django.utils import timezone
from datetime import timedelta


def is_editable(post, user):
    if user.is_superuser or 'microblog.can_edit' in user.get_group_permissions():
        return True

    if post.author.pk == user.pk:
        time0 = timezone.now()

        try:
            time1 = post.last_edited_date + timedelta(minutes=10)
            if time0 < time1:
                return True
        except TypeError:
            time2 = post.date + timedelta(minutes=10)
            if time0 < time2:
                return True

    return False