from django.contrib.auth.decorators import login_required
from views import *
from django.conf.urls import patterns, include, url
from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', AllPosts.as_view(), name='all_posts'),

    url(r'^filter/all/$', ChooseAll.as_view(), name='menu_all'),
    url(r'^filter/user/$', ChooseUser.as_view(), name='menu_user'),
    url(r'^filter/month/$', ChooseDate.as_view(), name='menu_date'),
    url(r'^filter/tag/$', ChooseTag.as_view(), name='menu_tag'),

    url(r'^filter/user/(?P<user_id>\d+)/$', UserPosts.as_view(), name='user_posts'),
    url(r'^filter/month/(?P<month_id>\d+)/$', DatePosts.as_view(), name='date_posts'),
    url(r'^filter/post/(?P<post_id>\d+)/$', ShowPost.as_view(), name='show_post'),
    url(r'^filter/tag/(?P<tag_id>\d+)/$', PostTags.as_view(), name='post_tags'),

    url(r'^add/$', login_required(AddPost.as_view()), name='add_post'),
    url(r'^edit/(?P<post_id>\d+)/$', login_required((EditPost.as_view())), name='edit_post'),

    url(r'^login/$', Login.as_view(), name='login'),
    url(r'^logout/$', login_required(Logout.as_view()), name='logout'),
)