from django.views.generic import View
from django.shortcuts import render, get_object_or_404
from forms import PostForm
from django.contrib.auth import login, logout
from methods import *
from django.contrib.auth.forms import AuthenticationForm
from cms.api import create_page
from cms.models.titlemodels import Title
from cms_plugins import *
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect


class AllPosts(View):
    def get(self, request):
        """
        Wypisywanie wszystkich wpisow z bazy
        """
        return render(request, 'microblog/show_posts.html', {'posts': Posts.objects.all().order_by('-id')})


class ChooseAll(View):
    def get(self, request):
        """
        Wypisywanie wszystkich wpisow z bazy do strony filtru ALL
        """
        return render(request, 'microblog/filterAll.html', {'posts': Posts.objects.all().order_by('-id')})


class ChooseTag(View):
    def get(self, request):
        """
        Wypisywanie wszystkich wpisow z bazy do strony filtru TAG
        """
        return render(request, 'microblog/filterTag.html', {'posts': Posts.objects.all().order_by('-id')})


class ChooseDate(View):
    def get(self, request):
        """
        Wypisywanie wszystkich wpisow z obecnego miesiace do strony filtru DATE
        """
        return render(request, 'microblog/filterMonth.html',
                      {'posts': Posts.objects.all().filter(date__month=timezone.now().month).order_by('-id')})


class ChooseUser(View):
    def get(self, request):
        """
        Wypisywanie wszystkich wpisow z dla zalogowanego uzytkownika
        lub wszystkich wpisow dla niezalogowanego do strony filtru USER
        """
        if request.user.is_authenticated():
            return render(request, 'microblog/filterUser.html',
                          {'posts': Posts.objects.all().filter(author=request.user).order_by('-id')})
        else:
            return render(request, 'microblog/filterUser.html', {'posts': Posts.objects.all().order_by('-id')})


class PostTags(View):
    def get(self, request, tag_id):
        """
        Wypisywanie wpisow z danym tagiem
        """
        return render(request, 'microblog/filterTag.html',
                      {'posts': Posts.objects.all().filter(tags__id=tag_id).order_by('-id')})


class UserPosts(View):
    def get(self, request, user_id):
        """
        Wypisywanie wpisow danego uzytkownika
        """
        return render(request, 'microblog/filterUser.html',
                      {'posts': Posts.objects.all().filter(author=User.objects.all().filter(id=user_id)).order_by('-id')})


class DatePosts(View):
    def get(self, request, month_id):
        """
        Wypisywanie wpisow z danym miesiacem publikacji
        """
        return render(request, 'microblog/filterMonth.html',
                      {'posts': Posts.objects.all().filter(date__month=month_id).order_by('-id')})


class ShowPost(View):
    def get(self, request, post_id):
        """
        Wypisywanie detali wpisu GET
        """
        return render(request, 'microblog/show_post.html', {'posts': Posts.objects.all().filter(pk=post_id)})

    def post(self, request, post_id):
        """
        Wypisywanie detali wpisu POST
        """
        return render(request, 'microblog/show_post.html', {'posts': Posts.objects.all().filter(pk=post_id)})


class AddPost(View):
    def get(self, request, *args, **kwargs):
        """
        Wyswietlanie formularza dodawania wpisow
        """
        return render(request, 'microblog/add_post.html', {
            'form': PostForm(initial={
                'author': request.user,
                'author_name': request.user.username,
                'date': timezone.now()
            })
        })

    def post(self, request, *args, **kwargs):
        """
        Walidowanie i dodawanie wpisu do bazy
        """
        form = PostForm(request.POST)
        if form.is_valid():
            form.save()

            title = Title.objects.all().filter(title='Blog').first()
            create_page(
                title=form.instance.title,
                language='pl',
                slug='/blog/filter/post/'+str(form.instance.pk)+'/',
                template='fullwidth.html',
                overwrite_url='/blog/filter/post/'+str(form.instance.pk)+'/',
                created_by=request.user,
                parent=title.page,
                publication_date=timezone.now(),
                in_navigation=False,
                published=True
            )

            return render(request, 'microblog/ok.html', {'text': 'Wpis dodany!'})
        else:
            return render(request, 'microblog/add_post.html', {'form': form})


class EditPost(View):
    def get(self, request, post_id):
        """
        Wyswietlanie formularza edycji wpisu
        """
        post = get_object_or_404(Posts, id=post_id)
        if is_editable(post, request.user):
            return render(request, 'microblog/edit_post.html', {'form': PostForm(instance=post), 'id': post.pk})
        else:
            return render(request, 'microblog/ok.html', {'text': 'Brak dostepu!'})

    def post(self, request, post_id):
        """
        Walidacja i zapisywanie efektow edycji wpisu
        """
        post = get_object_or_404(Posts, id=post_id)
        post.last_edited_by = request.user
        post.last_edited_date = timezone.now()
        form = PostForm(request.POST, instance=post)
        if is_editable(post, request.user):
            if form.is_valid():
                form.save()
                return render(request, 'microblog/ok.html', {'text': 'Wpis zaktualizowany!'})
            else:
                return render(request, 'microblog/edit_post.html', {'form': form, 'id': post_id})
        else:
            return render(request, 'microblog/ok.html', {'text': 'Brak dostepu!'})


class Login(View):
    def get(self, request):
        """
        Wyswietlanie formularza logowania
        """
        return render(request, 'microblog/login.html', {'form': AuthenticationForm()})

    def post(self, request):
        """
        Walidacja i logowanie uzytkownika
        """
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            login(request, form.get_user())
            return render(request, 'microblog/ok.html', {'text': 'Zalogowano!'})
        elif form.get_user() and not form.get_user().is_active:
            return render(request, 'microblog/login.html', {'error': 'Uzytkownik jest nieaktywny', 'form': AuthenticationForm()})
        else:
            return render(request, 'microblog/login.html', {'error': 'Zle haslo lub login!',
                                                            'form': AuthenticationForm()})


class Logout(View):
    def get(self, request, *args, **kwargs):
        """
        Wylogowanie uzytkownika
        """
        logout(request)
        return HttpResponseRedirect(reverse('login'))