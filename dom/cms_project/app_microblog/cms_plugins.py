from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from models import *
from django.utils.translation import ugettext as _

class CMSPostsPlugin(CMSPluginBase):
    model = PostsPlugin
    name = _("Post Plugin")
    render_template = "microblog/plugin_latest_news.html"
    cache = False

    def render(self, context, instance, placeholder):
        instance.refresh()
        context.update({'instance': instance})
        return context


class CMSTagsPlugin(CMSPluginBase):
    model = TagsPlugin
    name = _("Tag Plugin")
    render_template = "microblog/plugin_filter_tags.html"
    cache = False

    def render(self, context, instance, placeholder):
        instance.refresh()
        context.update({'instance': instance})
        return context


class CMSUserPlugin(CMSPluginBase):
    model = UserPlugin
    name = _("User Plugin")
    render_template = "microblog/plugin_filter_user.html"
    cache = False

    def render(self, context, instance, placeholder):
        instance.refresh()
        context.update({'instance': instance})
        return context


plugin_pool.register_plugin(CMSTagsPlugin)
plugin_pool.register_plugin(CMSPostsPlugin)
plugin_pool.register_plugin(CMSUserPlugin)
