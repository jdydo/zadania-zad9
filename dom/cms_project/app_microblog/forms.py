from django.forms import ModelForm, HiddenInput, PasswordInput, EmailField
from django import forms
from models import Posts
from django.contrib.auth.models import User


class PostForm(ModelForm):

    class Meta:
        model = Posts
        fields = [
            'author', 'title', 'text', 'tags', 'date'
        ]
        widgets = {
            'author': HiddenInput(),
            'date': HiddenInput(),
        }

    def clean_title(self):
        data = self.cleaned_data['title']
        if len(data) < 5 or len(data) > 50:
            raise forms.ValidationError("Tytul powinien miec od 5 do 50 znakow.")
        return data

    def clean_text(self):
        data = self.cleaned_data['text']
        if len(data) < 10 or len(data) > 1000:
            raise forms.ValidationError("Wpis powinien miec od 10 do 1000 znakow.")
        return data

    def clean_tags(self):
        data = self.cleaned_data['tags']
        if len(data) > 5:
            raise forms.ValidationError("Mozesz wybrac maksymalnie 5 tagow.")
        return data


class UserForm(ModelForm):

    class Meta:
        model = User
        fields = [
            'username', 'password', 'email'
        ]
        widgets = {
            'password': PasswordInput(),
        }

    def clean_username(self):
        data = self.cleaned_data['username']
        if len(data) < 5 or len(data) > 15:
            raise forms.ValidationError("Login powinien miec od 5 do 15 znakow.")
        elif User.objects.all().filter(username=data):
            raise forms.ValidationError("Uzytkownik juz istnieje.")
        return data

    def clean_password(self):
        data = self.cleaned_data['password']
        if len(data) < 5 or len(data) > 15:
            raise forms.ValidationError("Haslo powinno miec od 5 do 15 znakow.")
        return data

    def clean_email(self):
        data = self.cleaned_data['email']
        if not data:
            raise forms.ValidationError("To pole jest wymagane.")
        elif User.objects.all().filter(email=data):
            raise forms.ValidationError("Email juz zarejestrowany.")
        return data