# -*- coding: utf-8 -*-
from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from models import *
from django.utils.translation import ugettext as _


class CMSLikePlugin(CMSPluginBase):
    model = LikePlugin
    name = _("Like Plugin")
    render_template = "plugin_facebook_like/plugin_like.html"


plugin_pool.register_plugin(CMSLikePlugin)
