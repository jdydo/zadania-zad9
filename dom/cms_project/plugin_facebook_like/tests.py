# -*- coding=utf-8 -*-

from django.shortcuts import render_to_response
from django.test.utils import override_settings
from cms.test_utils.testcases import CMSTestCase
from cms_plugins import *
from models import *


@override_settings(ROOT_URLCONF='cms_project.test_urls')
class LikePluginTests(CMSTestCase):

    def setUp(self):
        self.plugin = CMSLikePlugin()

    def test_plugin_latest_news(self):
        context = {}
        instance = LikePlugin(href='localhost', share=True, show_faces=True, layout='box_count', action='like')
        html = render_to_response(self.plugin.render_template, self.plugin.render(context, instance, None))

        self.assertIn('class="fb-like"', str(html))
        self.assertIn('data-href="localhost"', str(html))
        self.assertIn('data-layout="box_count"', str(html))
        self.assertIn('data-action="like"', str(html))
        self.assertIn('data-show-faces="True"', str(html))
        self.assertIn('data-share="True"', str(html))