# -*- coding: utf-8 -*-
from django.db import models
from cms.models import CMSPlugin


class Contact(models.Model):
    first_name = models.CharField(max_length=50, verbose_name='Imię')
    last_name = models.CharField(max_length=50, verbose_name='Nazwisko')
    message = models.TextField(max_length=500, verbose_name='Wiadomość')
    sender = models.EmailField(verbose_name='E-mail')


class ContactPlugin(CMSPlugin):
    email = models.EmailField()
