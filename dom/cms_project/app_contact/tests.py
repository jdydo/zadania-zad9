# -*- coding=utf-8 -*-

from django.test.client import Client
from django.test.utils import setup_test_environment
from django.test.utils import override_settings
from cms.test_utils.testcases import CMSTestCase
from django.core import mail


setup_test_environment()
client = Client()


@override_settings(ROOT_URLCONF='cms_project.test_urls')
class ContactTest(CMSTestCase):

    def test_get_contact(self):
        response = client.get('/myapp2/')
        assert response.status_code == 200
        assert 'name="first_name"' in response.content
        assert 'name="last_name"' in response.content
        assert 'name="sender"' in response.content
        assert 'name="message"' in response.content

    def test_post_contact_valid(self):
        response = client.post('/myapp2/', {'first_name': 'test1', 'last_name': 'test2', 'sender': 'test@test.com', 'message': 'Wiadomosc testowa'}, follow=True)
        assert response.status_code == 200
        assert response.context['status'] == 1
        assert 'Twoja wiadomość została wysłana.' in response.content
        assert len(mail.outbox) == 2
        assert 'Wiadomosc od test1 test2' in mail.outbox[0].subject
        assert 'Wiadomosc testowa' in mail.outbox[0].body
        assert 'Potwierdzenie wysłania wiadomości' in mail.outbox[1].subject
        assert 'Wiadomosc testowa' in mail.outbox[1].body

    def test_post_contact_invalid(self):
        response = client.post('/myapp2/', {'first_name': '', 'last_name': '', 'sender': '', 'message': ''}, follow=True)
        assert response.status_code == 200
        assert 'To pole jest wymagane.' in str(response)