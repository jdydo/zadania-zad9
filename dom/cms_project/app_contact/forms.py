# -*- coding: utf-8 -*-

from django.forms import ModelForm
from django import forms
from models import Contact


class ContactForm(ModelForm):

    class Meta:
        model = Contact
        fields = [
            'first_name', 'last_name', 'sender', 'message'
        ]

    def clean_first_name(self):
        data = self.cleaned_data['first_name']
        if len(data) < 1 or len(data) > 50:
            raise forms.ValidationError("Twoje imię musi mieć przynajmniej 1 a najwięcej 50 znaków")
        return data

    def clean_last_name(self):
        data = self.cleaned_data['last_name']
        if len(data) < 1 or len(data) > 50:
            raise forms.ValidationError("Twoje nazwisko musi mieć przynajmniej 1 a najwięcej 50 znaków")
        return data

    def clean_message(self):
        data = self.cleaned_data['message']
        if len(data) < 1 or len(data) > 500:
            raise forms.ValidationError("Twoja wiadomość musi składać się z przynajmniej 1 a maksymalnie 500 znaków")
        return data