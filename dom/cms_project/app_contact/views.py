# -*- coding: utf-8 -*-

from django.views.generic import View
from django.shortcuts import render
from django.core.mail import send_mail
from forms import ContactForm
from django.utils import timezone


class Contact(View):
    def get(self, request):
        return render(request, "app_contact/contact.html", {'form': ContactForm})

    def post(self, request):
        form = ContactForm(request.POST)
        if form.is_valid():
            guardian1 = send_mail(
                'Wiadomosc od %s %s' % (form.instance.first_name, form.instance.last_name),
                form.instance.message,
                form.instance.sender,
                ['kiraqun7@gmail.com'],
                fail_silently=True
            )

            if guardian1 == 1:
                guardian2 = send_mail(
                    'Potwierdzenie wysłania wiadomości',
                    u'W dniu %s wysłałeś/aś do mnie wiadomość o następującej treści:\n%s' % (timezone.now(), form.instance.message),
                    'kiraqun7@gmail.com',
                    [form.instance.sender],
                    fail_silently=True
                )
            else:
                guardian2 = 0

            if guardian1 == 1 and guardian2 == 1:
                guardian = 1
            else:
                guardian = 0

            return render(request, "app_contact/contact.html", {'status': guardian, 'form': ContactForm})
        else:
            return render(request, "app_contact/contact.html", {'form': form})