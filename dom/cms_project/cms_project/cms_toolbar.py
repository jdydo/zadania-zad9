from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _
from cms.toolbar_pool import toolbar_pool
from cms.toolbar_base import CMSToolbar
from cms.toolbar.items import Break
from cms.cms_toolbar import ADMIN_MENU_IDENTIFIER, ADMINISTRATION_BREAK


@toolbar_pool.register
class BlogToolbar(CMSToolbar):

    def populate(self):
        admin_menu = self.toolbar.get_or_create_menu(ADMIN_MENU_IDENTIFIER, _('PWI'))
        position = admin_menu.find_first(Break, identifier=ADMINISTRATION_BREAK)
        menu = admin_menu.get_or_create_menu('blog-menu', _('Blog'), position=position)
        menu.add_sideframe_item(_('Wpisy'), url='/admin/app_microblog/posts/')
        menu.add_sideframe_item(_('Tagi'), url='/admin/app_microblog/tags/')
        menu.add_sideframe_item(_('Komentarze'), url='/admin/plugin_comment/comment/')
        admin_menu.add_break('blog-break', position=menu)