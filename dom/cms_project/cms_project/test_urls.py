from django.contrib import admin
from django.conf.urls import url, patterns, include


urlpatterns = patterns(
    '',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^myapp1/', include('app_microblog.urls')),
    url(r'^myapp2/', include('app_contact.urls')),
    url(r'', include('cms.urls')),
)
