# -*- coding: utf-8 -*-

from django.utils.translation import ugettext_lazy as _
from cms.menu_bases import CMSAttachMenu
from menus.base import NavigationNode
from menus.menu_pool import menu_pool


class AccountMenu(CMSAttachMenu):
    name = _("Account Menu")

    def get_nodes(self, request):
        nodes = []
        n1 = NavigationNode(_('Zaloguj'), "/blog/login/", 1, attr={'visible_for_authenticated': False})
        n2 = NavigationNode(_('Panel administracyjny'), "/admin/", 2, attr={'visible_for_anonymous': False})
        n3 = NavigationNode(_('Wyloguj'), "/blog/logout/", 3, attr={'visible_for_anonymous': False})
        nodes.append(n1)
        nodes.append(n2)
        nodes.append(n3)
        return nodes


menu_pool.register_menu(AccountMenu)