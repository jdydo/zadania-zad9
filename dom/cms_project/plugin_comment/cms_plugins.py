# -*- coding: utf-8 -*-
from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from models import *
from django.utils.translation import ugettext as _
from forms import CommentForm
from django.utils import timezone


class CMSWriteCommentPlugin(CMSPluginBase):
    name = _("Write Comment Plugin")
    render_template = "plugin_comment/plugin_write_comment.html"
    cache = False

    def render(self, context, instance, placeholder):
        request = context['request']
        current_page = request.current_page

        if not context.get('current_page') and not current_page.parent:
            context['status'] = 0
            return context

        if request.method == 'POST':
            form = CommentForm(request.POST)
            if form.is_valid() and form.instance.is_addable():
                    form.save()
                    context['status'] = 1
            elif form.instance.is_addable():
                context['form'] = form
            else:
                context['form'] = CommentForm(initial={'page_id': current_page.pk, 'date': timezone.now()})
        elif request.method == 'GET':
            context['form'] = CommentForm(initial={'page_id': current_page.pk, 'date': timezone.now()})
        return context


class CMSReadCommentPlugin(CMSPluginBase):
    model = ReadCommentPlugin
    name = _("Show Page Comment Plugin")
    render_template = "plugin_comment/plugin_read_comment.html"
    cache = False

    def render(self, context, instance, placeholder):
        request = context['request']
        current_page = request.current_page
        if not context.get('current_page') and not current_page.parent:
            context['status'] = 'fail'
        else:
            instance.refresh(current_page.pk)
            context.update({'instance': instance})
        return context


plugin_pool.register_plugin(CMSWriteCommentPlugin)
plugin_pool.register_plugin(CMSReadCommentPlugin)

