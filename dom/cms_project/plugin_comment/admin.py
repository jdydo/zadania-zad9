from django.contrib import admin
from models import Comment


class CommentAdmin(admin.ModelAdmin):
    list_display = ('author', 'text')
    list_filter = ('author', )


admin.site.register(Comment, CommentAdmin)