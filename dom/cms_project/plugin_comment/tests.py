# -*- coding=utf-8 -*-

from django.shortcuts import render_to_response
from django.test.utils import override_settings
from cms.test_utils.testcases import CMSTestCase
from cms_plugins import *
from models import *
from django.test.client import RequestFactory
from forms import *


@override_settings(ROOT_URLCONF='cms_project.test_urls')
class CommentPluginTests(CMSTestCase):

    def setUp(self):
        self.read_plugin = CMSReadCommentPlugin()
        self.write_plugin = CMSWriteCommentPlugin()
        self.request = RequestFactory()
        Comment(page_id=1, text='Bardzo Cie lubie', author='Jan', date=timezone.now()).save()
        Comment(page_id=1, text='Ja Ciebie tez', author='Janina', date=timezone.now()).save()

    def test_plugin_comment_read1(self):
        context = {'request': FakeRequest(1, None, 'GET'), 'current_page': FakeCurrentPage(1, None)}
        instance = ReadCommentPlugin()
        html = render_to_response(self.read_plugin.render_template, self.read_plugin.render(context, instance, None))

        self.assertIn('Bardzo Cie lubie', str(html))
        self.assertIn('Ja Ciebie tez', str(html))
        self.assertIn('Jan', str(html))
        self.assertIn('Janina', str(html))

    def test_plugin_comment_read_error(self):
        context = {'request': FakeRequest(1, None, 'GET'), 'current_page': None}
        instance = ReadCommentPlugin()
        html = render_to_response(self.read_plugin.render_template, self.read_plugin.render(context, instance, None))

        self.assertIn('Strona wiazaca z baza została usunieta! Wyswietlanie komentarzy wylaczone.', str(html))
        self.assertNotIn('Bardzo Cie lubie', str(html))
        self.assertNotIn('Ja Ciebie tez', str(html))
        self.assertNotIn('Jan', str(html))
        self.assertNotIn('Janina', str(html))

    def test_plugin_comment_write_get(self):
        context = {'request': FakeRequest(1, None, 'GET'), 'current_page': FakeCurrentPage(1, None)}
        instance = CMSWriteCommentPlugin()
        html = render_to_response(self.write_plugin.render_template, self.write_plugin.render(context, instance, None))
        self.assertIn('name="page_id"', str(html))
        self.assertIn('name="author"', str(html))
        self.assertIn('name="text"', str(html))
        self.assertIn('name="date"', str(html))

    def test_plugin_comment_write_post_valid(self):
        comment = Comment(page_id=1, text='Jakis tekst ciekawy', author='test123', date=timezone.now())
        context = {'request': FakeRequest(1, None, 'POST', comment), 'current_page': FakeCurrentPage(1, None), 'form': CommentForm(instance=comment)}
        instance = CMSWriteCommentPlugin()
        html = render_to_response(self.write_plugin.render_template, self.write_plugin.render(context, instance, None))
        self.assertIn('OK!', str(html))
        self.assertEqual(len(Comment.objects.all()), 3)

    def test_plugin_comment_write_post_invalid(self):
        comment = Comment(page_id=1, text='aa', author='bb', date=timezone.now())
        context = {'request': FakeRequest(1, None, 'POST', comment), 'current_page': FakeCurrentPage(1, None), 'form': CommentForm(instance=comment)}
        instance = CMSWriteCommentPlugin()
        html = render_to_response(self.write_plugin.render_template, self.write_plugin.render(context, instance, None))
        self.assertIn('przynajmniej 10 a maksymalnie 400', str(html))
        self.assertIn('przynajmniej 5 a maksymalnie 15', str(html))
        self.assertEqual(len(Comment.objects.all()), 2)

    def test_plugin_comment_write_error(self):
        context = {'request': FakeRequest(1, None, 'GET'), 'current_page': None}
        instance = CMSWriteCommentPlugin()
        html = render_to_response(self.write_plugin.render_template, self.write_plugin.render(context, instance, None))
        self.assertIn('Strona wiazaca z baza została usunieta! Wyswietlanie komentarzy wylaczone.', str(html))


class FakeRequest:
    def __init__(self, pk, parent, method, comment=Comment()):
        self.current_page = FakeCurrentPage(pk, parent)
        self.method = method
        self.POST = {'date': comment.date,
                     'text': comment.text,
                     'csrfmiddlewaretoken': 'QhuegHNeG3doaBT657SK3xmeLOZscOcq',
                     'page_id': comment.page_id,
                     'author': comment.author}


class FakeCurrentPage():
    def __init__(self, pk, parent):
        self.pk = pk
        self.parent = parent