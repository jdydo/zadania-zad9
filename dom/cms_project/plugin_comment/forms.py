# -*- coding: utf-8 -*-

from django.forms import ModelForm, HiddenInput
from django import forms
from models import Comment


class CommentForm(ModelForm):

    class Meta:
        model = Comment
        fields = [
            'page_id', 'author', 'text', 'date'
        ]
        widgets = {
            'page_id': HiddenInput(),
            'date': HiddenInput(),
        }

    def clean_text(self):
        data = self.cleaned_data['text']
        if len(data) < 10 or len(data) > 400:
            raise forms.ValidationError("Twój komentarz musi mieć przynajmniej 10 a maksymalnie 400 znaków")
        return data

    def clean_author(self):
        data = self.cleaned_data['author']
        if len(data) < 5 or len(data) > 15:
            raise forms.ValidationError("Twój nick musi składać się z przynajmniej 5 a maksymalnie 15 znaków")
        return data

