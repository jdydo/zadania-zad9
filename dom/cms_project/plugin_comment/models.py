from django.db import models
from cms.models import CMSPlugin
from cms.models import Page
from django.contrib import admin


class Comment(models.Model):
    page_id = models.IntegerField()
    text = models.TextField(verbose_name='Komentarz')
    author = models.CharField(max_length=30, verbose_name='Ksywka')
    date = models.DateTimeField(blank=True, null=True)

    def is_addable(self):
        if not Comment.objects.all().filter(page_id=self.page_id, text=self.text, author=self.author, date=self.date):
            return True
        return False


class ReadCommentPlugin(CMSPlugin):
    comments = Comment.objects.all()

    def refresh(self, page_id):
        self.comments = Comment.objects.all().filter(page_id=page_id).order_by('pk')